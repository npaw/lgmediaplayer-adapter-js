## [6.8.0] - 2022-01-24
### Added
- Check to start new view when it was previously stopped (background)
### Library
- Packaged with `lib 6.8.11`

## [6.5.0] - 2019-07-16
### Library
- Packaged with `lib 6.5.7`

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.3.0] - 2018-06-27
### Library
- Packaged with `lib 6.3.0`
