var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.LgMediaPlayer = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.playPosition / 1000
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.playTime / 1000
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var br = this.player.mediaPlayInfo().bitrateTarget
    if (br <= 0) {
      br = -1
    } else if (br < 524288) { 
      br *= 1000 // less than 0,5mbit? no, is reporting in kb, else: reported in bits, so it is ok
    }
    return br
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.data
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'LG-MediaPlayer'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
  },

  /** Listener for events. */
  playStateChangeHandler: function () {
    try {
      youbora.Log.debug('State Change: ' + this.player.playState)
      switch (this.player.playState) {
        case 0: // Stopped
          this.fireStop()
          break
        case 1: // Playing
          this.fireStart()
          this.fireResume()
          this.fireBufferEnd()
          this.fireSeekEnd()
          this.fireJoin()
          break
        case 2: // Paused
          this.firePause()
          break
        case 3: // Connecting
          this.fireStart()
          break
        case 4: // Buffering
          this.fireBufferBegin()
          break
        case 5: // Finished
          this.fireStop()
          break
        case 6: // Error
          this.fireError(this.player.error)
      }
    } catch (err) {
      youbora.Log.error(err)
    }
  }
})

module.exports = youbora.adapters.LgMediaPlayer
